package com.newfarm.alfanews.presentation.view.common

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.View
import android.widget.TextView
import io.realm.RealmViewHolder

class BaseRealmViewHolder : RealmViewHolder {
    private var binding: ViewDataBinding = DataBindingUtil.bind(itemView)!!

    constructor(itemView: View) : super(itemView) {
        binding = DataBindingUtil.bind(itemView)!!
    }

    constructor(headerTextView: TextView) : super(headerTextView)

    fun getBinding(): ViewDataBinding {
        return binding
    }
}