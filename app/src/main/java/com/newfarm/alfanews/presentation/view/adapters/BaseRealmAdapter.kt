package com.newfarm.alfanews.presentation.view.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mcxiaoke.koi.ext.onClick
import com.newfarm.alfanews.BR
import com.newfarm.alfanews.presentation.view.common.BaseRealmViewHolder
import com.newfarm.alfanews.presentation.viewmodel.common.RootViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.realm.*

open class BaseRealmAdapter<T: RealmObject>(val viewModel: RootViewModel, val holderLayout:Int,
                                            val data: RealmResults<T>, automaticUpdate: Boolean = true, animateResults: Boolean = true)
    : RealmBasedRecyclerViewAdapter<T, BaseRealmViewHolder>(viewModel.context, data, automaticUpdate, animateResults, null) {

    val disposable = CompositeDisposable()

    var onItemClick: PublishSubject<T> = PublishSubject.create()

    override fun onCreateRealmViewHolder(viewGroup: ViewGroup, viewType: Int): BaseRealmViewHolder {
        val itemView = LayoutInflater.from(viewModel.context).inflate(holderLayout, viewGroup, false)

        return BaseRealmViewHolder(itemView)
    }

    override fun onBindRealmViewHolder(holder: BaseRealmViewHolder?, position: Int) {
        val model = getItemByPosition(position)

        holder?.getBinding()?.setVariable(BR.viewModel, viewModel)
        holder?.getBinding()?.setVariable(BR.model, model)
        holder?.getBinding()?.setVariable(BR.context, viewModel.context)
        holder?.getBinding()?.setVariable(BR.viewModel, viewModel)
        holder?.getBinding()?.executePendingBindings()

        holder?.itemView?.onClick { v ->
            v.isClickable = false
            onItemClick.onNext(model)
            v.postDelayed({ v.isClickable = true }, 500)
        }
    }

    open fun getItemByPosition(position: Int): T {
        return realmResults[position]!!
    }

    override fun getLastItem(): Any? {
        return if (realmResults.size > 0) {
            this.realmResults[this.realmResults.size - 1]
        }
        else null
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        Log.d(this.javaClass.simpleName, String.format("Disposed %d items", disposable.size()))
        disposable.clear()
        super.onDetachedFromRecyclerView(recyclerView)
    }
}