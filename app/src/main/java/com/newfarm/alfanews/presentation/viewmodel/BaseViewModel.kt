package com.newfarm.alfanews.presentation.viewmodel

import android.databinding.Bindable
import android.os.Handler
import com.newfarm.alfanews.BR
import com.newfarm.alfanews.extentions.deleteFromRealmEx
import com.newfarm.alfanews.extentions.executeTransactionSafe
import com.newfarm.alfanews.presentation.model.ChannelEntity
import com.newfarm.alfanews.presentation.model.News
import com.newfarm.alfanews.presentation.viewmodel.common.RootViewModel
import com.newfarm.alfanews.remote.NewsAPI
import com.newfarm.alfanews.remote.common.RSS_NEWS_CATEGORY
import com.newfarm.alfanews.remote.common.RSS_NEWS_CITY
import com.newfarm.alfanews.remote.common.RSS_NEWS_SUBTYPE
import io.reactivex.rxkotlin.addTo

open class BaseViewModel : RootViewModel() {

    private val mNewsAPI = NewsAPI()

    @get:Bindable
    val newsData: ChannelEntity?
        get() {
            return realm.where(ChannelEntity::class.java).findFirst()
        }

    fun requestNews(executeContext: (Boolean) -> Unit, refresh: Boolean = false) {
        if (newsData != null && !refresh) {
            executeContext(true)
        }
        else {
            mNewsAPI.fetchRssNews(
                RSS_NEWS_SUBTYPE,
                RSS_NEWS_CATEGORY,
                RSS_NEWS_CITY
            )
                .map { response ->

                    val favouriteNewsIds: MutableList<Int?> = newsData?.news?.filter { it.isFavourite == true && it.guidSuffix != null }?.map { it.guidSuffix }?.let {
                        if(it is MutableList<Int?>) { it } else { mutableListOf() }
                    } ?: mutableListOf()

                   response.channel?.news?.forEach { news ->
                       /*** Получаю id новости  из номера html страницы ***/
                       news.link?.lastIndexOf("/")?.let { index ->
                           news.guidSuffix = news.link?.substring(index + 1)?.split(".")?.firstOrNull()?.toIntOrNull()
                       }
                       /*** Помечаю новость как понравившуюся, если её айдишник есть в списке айдишников уже сохранённых новостей ***/
                       if (favouriteNewsIds.contains(news.guidSuffix)) {
                           news.isFavourite = true
                           favouriteNewsIds.remove(news.guidSuffix)
                       }
                   }
                    /*** Добавляю список понравившихся новостей, которые не содержатся в новых новостях,
                     * к списку новых новостей перед тем, как перезаписать модель Channel в базе ***/
                    response.channel?.news?.addAll(realm.copyFromRealm(realm.where(News::class.java).`in`("guidSuffix", favouriteNewsIds.toTypedArray()).findAll()))
                    response
                }
                .map { response ->
                realm.executeTransactionSafe {
                    newsData?.deleteFromRealmEx()
                    realm.copyToRealm(response.channel)
                }
                true
            }.subscribe({
                notifyPropertyChanged(BR.newsData)
                executeContext(true)
            },{
                //error.onNext(it)
                executeContext(false)
            })
                .addTo(disposable)
        }
    }

    /*** Синхронизация данных каждые 5 мин ***/
    fun doDelaySync() {
        requestNews({}, true)
        Handler().postDelayed( { doDelaySync() }, 5 * 60 * 1000)
    }
}