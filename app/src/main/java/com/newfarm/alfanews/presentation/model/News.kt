package com.newfarm.alfanews.presentation.model

import io.realm.RealmObject
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item")
open class News: RealmObject() {

	var guidSuffix: Int? = null

	var isFavourite: Boolean? = null

	@get:Element(name = "title", required = false)
	@set:Element(name = "title", required = false)
	var title: String? = null

	@get:Element(name = "link", required = false)
	@set:Element(name = "link", required = false)
	var link: String? = null

	@get:Element(name = "description", required = false)
	@set:Element(name = "description", required = false)
	var description: String? = null
}