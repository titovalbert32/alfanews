package com.newfarm.alfanews.presentation.view.common

interface IDialog {
    fun showProgressDialog(message: String = "")
    fun hideProgressDialog()
}