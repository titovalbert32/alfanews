package com.newfarm.alfanews.presentation.model

import io.realm.RealmObject
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import com.newfarm.alfanews.extentions.RealmCascadeDelete
import com.newfarm.alfanews.extentions.deleteFromRealmEx
import io.realm.RealmList

@Root(name = "channel")
open class ChannelEntity: RealmObject(), RealmCascadeDelete {

	@get:Element(name = "title")
	@set:Element(name = "title")
	var title: String? = null

	@get:Element(name = "link")
	@set:Element(name = "link")
	var link: String? = null

	@get:Element(name = "description")
	@set:Element(name = "description")
	var description: String? = null

	@get:Element(name = "language")
	@set:Element(name = "language")
	var language: String? = null

	@get:Element(name = "pubDate")
	@set:Element(name = "pubDate")
	var pubDate: String? = null

	@get:Element(name = "docs")
	@set:Element(name = "docs")
	var documents: String? = null

	@get:Element(name = "managingEditor")
	@set:Element(name = "managingEditor")
	var managingEditor: String? = null

	@get:Element(name = "webMaster")
	@set:Element(name = "webMaster")
	var webMaster: String? = null

	@get:ElementList(name = "item", required = false, inline = true)
	@set:ElementList(name = "item", required = false, inline = true)
	var news: RealmList<News>? = null

	override fun cascadeDelete() {
		news?.deleteFromRealmEx()
	}
}