package com.newfarm.alfanews.presentation.view

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.newfarm.alfanews.R

class ExpandArrowAnimated : ImageView {

	var isReversed = false

	var isExpanded: Boolean? = null
		set(value) {
			if (field == null) {
				this.rotation = if(isReversed) 270f else 90f
				field = false
				return
			}
			if (field != value) {
				if (value != null && value) {
					this.startAnimation(AnimationUtils.loadAnimation(context, R.anim.expand_arrow_rotation))
					this.rotation = if (isReversed) 90f else 270f
				}
				else {
					this.startAnimation(AnimationUtils.loadAnimation(context, R.anim.collapse_arrow_rotation))
					this.rotation = if (isReversed) 270f else 90f
				}
				field = value
			}
		}

	constructor(context: Context, attrs: AttributeSet) : super (context, attrs, 0) {
		init()
	}
	constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
		init()
	}
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
		init()
	}

	private fun init() {
		this.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_right_arrow))
	}
}