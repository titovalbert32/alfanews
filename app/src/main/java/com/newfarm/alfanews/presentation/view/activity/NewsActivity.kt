package com.newfarm.alfanews.presentation.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.afollestad.materialdialogs.MaterialDialog
import com.newfarm.alfanews.R
import com.newfarm.alfanews.presentation.view.fragment.NewsCurrentFragment
import com.newfarm.alfanews.presentation.view.common.BaseAppCompatActivity
import com.newfarm.alfanews.presentation.view.adapters.TabsAdapter
import com.newfarm.alfanews.presentation.view.fragment.NewsFavouriteFragment
import com.newfarm.alfanews.presentation.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.news_activity.*
import kotlin.system.exitProcess

class NewsActivity : BaseAppCompatActivity() {

	val viewModel = NewsViewModel()
	private lateinit var tabAdapter: TabsAdapter

	companion object {

		private const val COMPETITOR_TYPE_TABS_COUNT: Int = 2

		fun start(ctx: Activity) {
			val intent = Intent(ctx, NewsActivity::class.java)
			ctx.startActivity(intent)
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.news_activity)

		val toolbar = findViewById<Toolbar>(R.id.toolbar)
		setSupportActionBar(toolbar)

		tabAdapter = TabsAdapter(supportFragmentManager, COMPETITOR_TYPE_TABS_COUNT)
		tabAdapter.addFragment(NewsCurrentFragment.newInstance(), resources.getString(R.string.current_news_title))
		tabAdapter.addFragment(NewsFavouriteFragment.newInstance(), resources.getString(R.string.favourite_news_title))

		viewPager.adapter = tabAdapter
		tabLayout.setupWithViewPager(viewPager)
	}

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		finish()
		return super.onOptionsItemSelected(item)
	}

	override fun onBackPressed() {
		MaterialDialog.Builder(this)
			.content(R.string.dialog_asc_exit_program)
			.positiveText(R.string.dialog_button_yes)
			.onPositive { dialog, _ ->

				dialog.dismiss()
				super.onBackPressed()
				finishAffinity()
				exitProcess(0)
			}
			.negativeText(R.string.dialog_button_no)
			.show()
	}
}