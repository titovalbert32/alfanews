package com.newfarm.alfanews.presentation.view.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.newfarm.alfanews.R
import com.newfarm.alfanews.databinding.NewsInfoActivityBinding
import com.newfarm.alfanews.presentation.view.adapters.TabsAdapter
import com.newfarm.alfanews.presentation.view.common.DataBindingActivity
import com.newfarm.alfanews.presentation.view.fragment.NewsInfoFragment
import com.newfarm.alfanews.presentation.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.news_activity.*

class NewsInfoActivity : DataBindingActivity<NewsInfoActivityBinding, NewsViewModel>() {

    companion object {
        private const val NEWS_GUID = "news_guid"

        fun start(ctx: Activity, guidSuffix: Int?) {
            val intent = Intent(ctx, NewsInfoActivity::class.java)
            guidSuffix?.let {
                intent.putExtra(NEWS_GUID, guidSuffix)
            }
            ctx.startActivity(intent)
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.news_info_activity
    }

    override fun onInit() {
        super.onInit()
        this.viewModel = NewsViewModel()

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val tabAdapter = TabsAdapter(supportFragmentManager, viewModel.newsList.count())
        viewModel.newsList.forEach { news ->
            tabAdapter.addFragment(NewsInfoFragment.newInstance(news.guidSuffix, news.link), news.title ?: "Новость")
        }
        viewPager.adapter = tabAdapter

        val guid = intent.extras?.getInt(NEWS_GUID, -1)
        val currentItem = viewModel.getNewsById(guid)
        val currentPos: Int? = if (currentItem != null) { viewModel.newsList.indexOf(currentItem) } else { null }
        if (currentPos != null) {
            viewPager.currentItem = currentPos
        }
    }

    override fun onBinding(binding: NewsInfoActivityBinding) {
        binding.viewModel = this.viewModel
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return true
    }
}