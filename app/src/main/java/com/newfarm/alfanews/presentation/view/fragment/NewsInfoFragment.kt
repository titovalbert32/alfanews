package com.newfarm.alfanews.presentation.view.fragment

import com.newfarm.alfanews.R
import com.newfarm.alfanews.databinding.NewsInfoFragmentBinding
import com.newfarm.alfanews.presentation.view.common.DataBindingFragment
import com.newfarm.alfanews.presentation.viewmodel.NewsViewModel

class NewsInfoFragment  : DataBindingFragment<NewsInfoFragmentBinding, NewsViewModel>() {

    private var mGuid: Int? = null
    var mLink: String? = null

    companion object {
        fun newInstance(guidSuffix: Int?, link: String?): NewsInfoFragment {
            val fragment = NewsInfoFragment()
            fragment.mGuid = guidSuffix
            fragment.mLink = link
            return fragment
        }
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.news_info_fragment
    }

    override fun onBinding(binding: NewsInfoFragmentBinding) {
        binding.viewModel = viewModel
        binding.context = this
    }

    override fun onInit() {
        super.onInit()
        this.viewModel = NewsViewModel()
    }
}