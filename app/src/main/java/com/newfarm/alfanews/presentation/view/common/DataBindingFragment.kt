package com.newfarm.alfanews.presentation.view.common

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.util.Log
import android.view.*
import com.afollestad.materialdialogs.MaterialDialog
import com.mcxiaoke.koi.ext.longToast
import com.newfarm.alfanews.R
import com.newfarm.alfanews.presentation.viewmodel.common.RootViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

abstract class DataBindingFragment<B: ViewDataBinding, VM: RootViewModel> : BaseFragment(), IDialog {
    lateinit protected var binding:B
        private set
    lateinit protected var viewModel:VM

    private var dialogBox: MaterialDialog? = null

    protected val disposable = CompositeDisposable()

    /*** Подписка, которая отменится при cancel'e progressDialog'а ***/
    protected val disposableDialog = CompositeDisposable()
    private var isStarting = true

    abstract fun getLayoutResourceId() : Int
    abstract fun onBinding(binding: B)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        this.binding = DataBindingUtil.inflate(inflater, getLayoutResourceId(), container, false)

        onInit()

        viewModel.title.subscribe {
            activity!!.title = it
        }.addTo(disposable)

        viewModel.network.subscribe {
            if (it) {
                showProgressDialog()
            }
            else {
                hideProgressDialog()
            }
        }.addTo(disposable)

        viewModel.error.subscribe {
            hideProgressDialog()
            val message = when {
                it.localizedMessage != null -> it.localizedMessage
                it.message != null -> it.message!!
                else -> it.toString()
            }
            longToast(message)
        }.addTo(disposable)

        viewModel.onStartFragment(context!!, this)

        onBinding(this.binding)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (getOptionMenuId() != null) {
            inflater?.inflate(getOptionMenuId()!!, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    open fun getOptionMenuId() : Int? {
        return null
    }

    open fun onInit() {}

    open fun onLoad() {

    }

    override fun onDestroyView() {
        Log.d(this.javaClass.simpleName, String.format("Disposed %d items, %d dialog items ", disposable.size(), disposableDialog.size()))
        viewModel.onStop()
        disposableDialog.clear()
        disposable.clear()

        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()

        if (isStarting) {
            onLoad()
            isStarting = false
        }
    }

    override fun showProgressDialog(message: String) {
        val activity = ctx as BaseAppCompatActivity
        hideProgressDialog()
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        dialogBox = MaterialDialog.Builder(ctx!!)
                .content(if (message.isEmpty()) getString(R.string.message_please_wait) else message)
                .progress(true, 0)
                .show()
    }

    override fun hideProgressDialog() {
        val activity = ctx as BaseAppCompatActivity
        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        if (dialogBox != null) {
            disposableDialog.clear()
            viewModel.progressDialogShowingDisposable.clear()
            dialogBox?.dismiss()
        }
    }
}