package com.newfarm.alfanews.presentation.view.fragment

import com.mcxiaoke.koi.ext.longToast
import com.newfarm.alfanews.R
import com.newfarm.alfanews.databinding.NewsCurrentFragmentBinding
import com.newfarm.alfanews.presentation.view.activity.NewsInfoActivity
import com.newfarm.alfanews.presentation.view.common.DataBindingFragment
import com.newfarm.alfanews.presentation.viewmodel.NewsViewModel
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.news_current_fragment.*

class NewsCurrentFragment : DataBindingFragment<NewsCurrentFragmentBinding, NewsViewModel>() {

	companion object {
		fun newInstance(): NewsCurrentFragment {
			return NewsCurrentFragment()
		}
	}

	override fun getLayoutResourceId(): Int {
		return R.layout.news_current_fragment
	}

	override fun onBinding(binding: NewsCurrentFragmentBinding) {
		binding.viewModel = viewModel
		binding.currentNewsList.setOnRefreshListener {
			viewModel.requestNews({
				currentNewsList.setRefreshing(false)
				if (!it)  {
					longToast(resources.getString(R.string.news_fetching_failed))
				}
			}, true)
		}
	}

	override fun onInit() {
		super.onInit()
		this.viewModel = NewsViewModel()
		viewModel.onNewsClick.subscribe { guid ->
			NewsInfoActivity.start(activity!!,
				guidSuffix =  guid)
		}.addTo(disposable)
	}
}
