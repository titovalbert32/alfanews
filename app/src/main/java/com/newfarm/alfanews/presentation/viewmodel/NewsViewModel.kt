package com.newfarm.alfanews.presentation.viewmodel

import android.databinding.Bindable
import com.newfarm.alfanews.BR
import com.newfarm.alfanews.R
import com.newfarm.alfanews.extentions.executeTransactionSafe
import com.newfarm.alfanews.presentation.model.News
import com.newfarm.alfanews.presentation.view.adapters.BaseRealmAdapter
import com.newfarm.alfanews.presentation.viewmodel.common.ISetFavourite
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject
import io.realm.RealmResults

class NewsViewModel : BaseViewModel(), ISetFavourite {

    val onNewsClick: PublishSubject<Int> = PublishSubject.create()

    @get:Bindable
    val newsList: RealmResults<News>
        get() {
            return realm.where(News::class.java).findAll()
        }

    @get:Bindable
    var newsAdapter: BaseRealmAdapter<News>? = null
        get() {
            if (field == null) {
                val adapter = BaseRealmAdapter(this, R.layout.news_list_item, newsList,
                    automaticUpdate = true,
                    animateResults = false
                )
                adapter.onItemClick.subscribe { model ->
                    if (model.guidSuffix != null) {
                        onNewsClick.onNext(model.guidSuffix!!)
                    }
                }.addTo(disposable)
                return adapter
            }
            return field
        }

    override fun setFavourite(model: News) {
        realm.executeTransactionSafe {
            model.isFavourite = !(model.isFavourite ?: false)
        }
    }

    fun getNewsById(guid: Int?): News? {
        return newsList.firstOrNull { it.guidSuffix == guid }
    }

    @get:Bindable
    var blocksExpanded: MutableSet<Int>? = null
        get() {
            if (field == null) {
                field = mutableSetOf()
            }
            return field
        }

    private fun blocksExpanded(position: Int) : Boolean {
        return blocksExpanded!!.contains(position)
    }

    fun onBlocksExpand(position: Int) {
        if (blocksExpanded(position)) {
            blocksExpanded!!.remove(position)
        }
        else {
            blocksExpanded!!.add(position)
        }
        notifyPropertyChanged(BR.blocksExpanded)
    }
}