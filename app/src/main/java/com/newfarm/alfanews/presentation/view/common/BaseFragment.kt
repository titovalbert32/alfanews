package com.newfarm.alfanews.presentation.view.common

import android.os.Bundle
import android.support.v4.app.Fragment
import android.content.Context

open class BaseFragment: Fragment() {
    protected val TAG = this.javaClass.simpleName

    protected var ctx: Context? = null
        private set

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        ctx = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }
}