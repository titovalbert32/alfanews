package com.newfarm.alfanews.presentation.viewmodel.common

import com.newfarm.alfanews.presentation.model.News

interface ISetFavourite {
    fun setFavourite(model: News)
}