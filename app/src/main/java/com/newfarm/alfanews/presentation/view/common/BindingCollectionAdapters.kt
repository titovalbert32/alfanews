package com.newfarm.alfanews.presentation.view.common

import android.databinding.BindingAdapter
import android.os.Build
import android.text.Html
import android.widget.TextView
import android.webkit.WebView
import com.newfarm.alfanews.presentation.view.ExpandArrowAnimated
import com.newfarm.alfanews.presentation.view.adapters.BaseRealmAdapter
import net.cachapa.expandablelayout.ExpandableLayout

object BindingCollectionAdapters {

    @BindingAdapter("adapter")
    @JvmStatic fun setAdapter(view: BaseRealmRecyclerView, adapter: BaseRealmAdapter<*>?) {
        if (adapter != null) {
            view.setAdapter(adapter)
        }
    }

    @BindingAdapter("app:html")
    @JvmStatic fun setHtmlText(view: TextView, value: String?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            view.text = Html.fromHtml(value, Html.FROM_HTML_MODE_COMPACT)
        } else {
            view.text = Html.fromHtml(value)
        }
    }

    @BindingAdapter("url")
    @JvmStatic fun setLoadUrl (view: WebView?, url: String?) {
        if (url != null) {
            view?.loadUrl(url)
        }
    }

    @BindingAdapter("app:layoutExpanded")
    @JvmStatic fun setLayoutExpanded(view: ExpandableLayout, expanded: Boolean) {
        if (expanded) {
            view.expand()
        } else {
            view.collapse()
        }
    }

    @BindingAdapter("isExpanded")
    @JvmStatic fun setIsExpanded(view: ExpandArrowAnimated, isExpanded: Boolean?) {
        view.isExpanded = isExpanded
    }
}