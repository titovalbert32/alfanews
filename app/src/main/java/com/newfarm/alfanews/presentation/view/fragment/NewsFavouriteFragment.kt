package com.newfarm.alfanews.presentation.view.fragment

import com.newfarm.alfanews.R
import com.newfarm.alfanews.databinding.NewsFavouriteFragmentBinding
import com.newfarm.alfanews.presentation.view.activity.NewsInfoActivity
import com.newfarm.alfanews.presentation.view.common.DataBindingFragment
import com.newfarm.alfanews.presentation.viewmodel.NewsFavouriteViewModel
import io.reactivex.rxkotlin.addTo

class NewsFavouriteFragment : DataBindingFragment<NewsFavouriteFragmentBinding, NewsFavouriteViewModel>() {

	companion object {
		fun newInstance(): NewsFavouriteFragment {
			return NewsFavouriteFragment()
		}
	}

	override fun getLayoutResourceId(): Int {
		return R.layout.news_favourite_fragment
	}

	override fun onBinding(binding: NewsFavouriteFragmentBinding) {
		binding.viewModel = viewModel
	}

	override fun onInit() {
		super.onInit()
		this.viewModel = NewsFavouriteViewModel()
		viewModel.onNewsClick.subscribe { guid ->
			NewsInfoActivity.start(activity!!,
				guidSuffix =  guid)
		}.addTo(disposable)
	}
}
