package com.newfarm.alfanews.presentation.view.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class TabsAdapter(fragmentManager: FragmentManager, private val pagesCount: Int) : FragmentPagerAdapter(fragmentManager) {
	private val mFragmentList = arrayListOf<Fragment>()
	private val mFragmentTitleList = arrayListOf<String>()

	override fun getItem(position: Int): Fragment {
		return mFragmentList[position]
	}

	fun addFragment(fragment: Fragment, title: String) {
		mFragmentList.add(fragment)
		mFragmentTitleList.add(title)
	}

	override fun getPageTitle(position: Int): CharSequence? {
		return mFragmentTitleList[position]
	}

	override fun getCount(): Int {
		return pagesCount
	}
}