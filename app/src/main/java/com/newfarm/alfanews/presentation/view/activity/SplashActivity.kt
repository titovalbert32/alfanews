package com.newfarm.alfanews.presentation.view.activity

import com.afollestad.materialdialogs.MaterialDialog
import com.mcxiaoke.koi.ext.longToast
import com.newfarm.alfanews.R
import com.newfarm.alfanews.databinding.SplashActivityBinding
import com.newfarm.alfanews.presentation.view.common.DataBindingActivity
import com.newfarm.alfanews.presentation.viewmodel.BaseViewModel
import kotlin.system.exitProcess

class SplashActivity : DataBindingActivity<SplashActivityBinding, BaseViewModel>() {

    override fun onInit() {
        super.onInit()
        this.viewModel = BaseViewModel()
    }

    override fun onBinding(binding: SplashActivityBinding) {
        binding.viewModel = this.viewModel
    }

    override fun getLayoutResourceId(): Int {
        return R.layout.splash_activity
    }

    override fun onBackPressed() {
        MaterialDialog.Builder(this)
            .content(R.string.dialog_asc_exit_program)
            .positiveText(R.string.dialog_button_yes)
            .onPositive { dialog, _ ->

                viewModel.disposable.clear()
                viewModel.progressDialogShowingDisposable.clear()

                dialog.dismiss()
                super.onBackPressed()
                finishAffinity()
                exitProcess(0)
            }
            .negativeText(R.string.dialog_button_no)
            .show()
    }

    override fun onLoad() {
        viewModel.requestNews({
            if (it) {
                viewModel.doDelaySync()
                NewsActivity.start(this)
            }
            else {
                longToast(resources.getString(R.string.news_fetching_failed))
            }
        })
    }
}

