package com.newfarm.alfanews.presentation.view.common

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.MotionEvent
import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView

class BaseRealmRecyclerView : RealmRecyclerView {

    constructor(context: Context): super(context) {
        init()
    }
    constructor(context: Context, attrs: AttributeSet): super(context, attrs){
        init()
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int): super(context, attrs, defStyleAttr){
        init()
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, bufferItems: Int): super(context, attrs, defStyleAttr, bufferItems){
        init()
    }

    private fun init() {
        if (this.recycleView.layoutManager is LinearLayoutManager) {
            this.recycleView.layoutManager = WrapLinearLayoutManager(this.context)
        }
    }

    var isListClickable = true

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (isListClickable) {
            super.dispatchTouchEvent(ev)
        }
        else {
            true
        }
    }
}