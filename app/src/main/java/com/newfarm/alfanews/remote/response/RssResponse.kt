package com.newfarm.alfanews.remote.response

import com.newfarm.alfanews.presentation.model.ChannelEntity
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "rss")
class RssResponse {

	@get:Attribute(name = "version")
	@set:Attribute(name = "version")
	var version: String? = null

	@get:Element(name = "channel")
	@set:Element(name = "channel")
	var channel: ChannelEntity? = null
}