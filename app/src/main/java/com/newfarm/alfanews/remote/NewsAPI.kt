package com.newfarm.alfanews.remote

import com.newfarm.alfanews.remote.common.ServerAPI
import com.newfarm.alfanews.remote.interfaces.INewsAPI
import com.newfarm.alfanews.remote.response.RssResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsAPI : ServerAPI<INewsAPI>(INewsAPI::class)  {

    fun fetchRssNews(subtype: Int, category: Int, city: Int) : Observable<RssResponse> {
        return get().fetchRssNews(subtype, category, city)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}