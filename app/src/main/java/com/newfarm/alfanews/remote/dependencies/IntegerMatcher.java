package com.newfarm.alfanews.remote.dependencies;

import org.simpleframework.xml.transform.Transform;

public class IntegerMatcher  implements Transform<Integer> {

    public IntegerMatcher() {}

    @Override
    public Integer read(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        else {
            return Integer.valueOf(value);
        }
    }

    @Override
    public String write(Integer value) {
        return value == null ? "" : value.toString();
    }
}