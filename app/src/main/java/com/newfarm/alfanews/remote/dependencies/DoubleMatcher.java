package com.newfarm.alfanews.remote.dependencies;

import org.simpleframework.xml.transform.Transform;

public class DoubleMatcher  implements Transform<Double> {
    public DoubleMatcher() {}

    @Override
    public Double read(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        else {
            return Double.valueOf(value);
        }
    }

    @Override
    public String write(Double value) {
        return value == null ? "" : value.toString();
    }
}