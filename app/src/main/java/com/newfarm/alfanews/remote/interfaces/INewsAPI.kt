package com.newfarm.alfanews.remote.interfaces

import com.newfarm.alfanews.remote.response.RssResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface INewsAPI {

    @GET("_/rss/_rss.html")
    fun fetchRssNews(@Query("subtype") subtype: Int,
                     @Query("category") category: Int,
                     @Query("city") city: Int): Observable<RssResponse>
}