package com.newfarm.alfanews.remote.common

import com.newfarm.alfanews.remote.dependencies.SimpleXmlConverterFactory2
import com.newfarm.alfanews.remote.dependencies.SimpleXmlMatcher
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass

open class ServerAPI<S:Any>(private val service: KClass<S>) {

    private fun httpClientBuilder(timeout:Long = 60): OkHttpClient.Builder {
        val builder = OkHttpClient.Builder()
        builder.protocols(Collections.singletonList(Protocol.HTTP_1_1))
        builder.readTimeout(timeout, TimeUnit.SECONDS)
        builder.connectTimeout(timeout, TimeUnit.SECONDS)
        builder.writeTimeout(timeout, TimeUnit.SECONDS)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(logging)

        return builder
    }

    private val nullOnEmptyConverterFactory = object : Converter.Factory() {
        fun converterFactory() = this
        override fun responseBodyConverter(type: Type, annotations: Array<out Annotation>, retrofit: Retrofit) = object : Converter<ResponseBody, Any?> {
            val nextResponseBodyConverter = retrofit.nextResponseBodyConverter<Any?>(converterFactory(), type, annotations)
            override fun convert(value: ResponseBody) =
                if (value.contentLength() != 0L) {
                    nextResponseBodyConverter.convert(value)
                }
                else null
        }
    }

    protected fun get(timeout:Long = 60): S {
        val client = httpClientBuilder(timeout)

        val strategy = AnnotationStrategy()
        val serializer = Persister(strategy, SimpleXmlMatcher.instance())

        val builder = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(nullOnEmptyConverterFactory)

        builder.addConverterFactory(SimpleXmlConverterFactory2.createNonStrict(serializer))

        val retrofit = builder.client(client.build()).build()
        return retrofit.create(service.java)
    }
}