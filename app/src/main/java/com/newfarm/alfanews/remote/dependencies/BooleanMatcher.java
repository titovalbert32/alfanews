package com.newfarm.alfanews.remote.dependencies;

import org.simpleframework.xml.transform.Transform;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BooleanMatcher implements Transform<Boolean> {
    @Override
    public Boolean read(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        else {
            value = value.toLowerCase();
            Set<String> trueSet = new HashSet(Arrays.asList("1", "true", "yes"));
            Set<String> falseSet = new HashSet(Arrays.asList("0", "false", "no"));

            if (trueSet.contains(value)) {
                return true;
            } else if (falseSet.contains(value)) {
                return false;
            }
            else {
                return null;
            }
        }
    }

    @Override
    public String write(Boolean value) throws Exception {
        return value == null ? "" : value.toString();
    }
}
