package com.newfarm.alfanews.remote.dependencies;

import org.simpleframework.xml.transform.RegistryMatcher;

public class SimpleXmlMatcher {

    static public RegistryMatcher instance() {
        RegistryMatcher result = new RegistryMatcher();

        result.bind(Integer.class, new IntegerMatcher());
        result.bind(Double.class, new DoubleMatcher());
        //result.bind(Date.class, new DateMatcher());
        result.bind(Boolean.class, new BooleanMatcher());

        return result;
    }
}
